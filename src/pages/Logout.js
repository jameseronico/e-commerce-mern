import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';



export default function Logout() {

	

	

	// Consume the UserContext object and destructure it to access the user state and unsetUser function from the context provider
	const { setUser } = useContext(UserContext)

	// Clear the localStorage of the user's information
	

	const unsetUser = () => {
		localStorage.removeItem('isAdmin')
		localStorage.removeItem('id')
		localStorage.removeItem('token')
	  }
	

	useEffect (() => {

		unsetUser()

		setUser({id: null})
	}, []);

	// Redirect back to log in
	return(

		<Navigate to='/login' />

	)
}

