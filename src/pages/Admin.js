import React, { useContext, useState } from 'react';
import { Row } from 'react-bootstrap';
import UserContext from '../UserContext';
import { Navigate,  useNavigate } from 'react-router-dom';
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';

import CreateProduct from '../components/CreateProduct'
import AllProduct from '../components/AllProducts'
import AllOrders from '../components/AllOrders'
import AllUsers from '../components/AllUsers'
import UpdateProduct from '../components/UpdateProduct'



import 'bootstrap/dist/css/bootstrap.min.css';


export default function AdminDashboard() {

  const navigate = useNavigate();

  const [key, setKey] = useState('create');

  const { user } = useContext(UserContext);

    if(user.isAdmin !== false ){

      

        return (

       
        
        <Row className="admin-page mt-5 pt-5">
          <div className="mt-5 pt-5">
            <h1 className="text-center">Welcome to the Admin Dashboard</h1>
          </div>
          <div className="text-warning">
            <Tabs
                id="controlled-tab-example"
                activeKey={key}
                onSelect={(k) => setKey(k)}
                className="mb-3 justify-content-center tab-page"
              >
                <Tab eventKey="create" title="Create Product">
                  <CreateProduct />
                </Tab>
                <Tab eventKey="viewall" title="View All Products">
                  <AllProduct />
                </Tab>
                <Tab eventKey="orders" title="View All Orders">
                <AllOrders />
                </Tab>
                <Tab eventKey="viewusers" title="View All Users">
                <AllUsers />
                </Tab>
                
               
            </Tabs>
          </div> 
        </Row>
        )}
    
}
