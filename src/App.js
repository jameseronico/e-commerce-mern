// app.js ( child compoments, things to add in the app) > index.js w/ app > index.html through (document.getElementById('root')) to body <div id="root"></div>.
// built-in imports
import { useState, useEffect } from 'react'; 
 
// package imports
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import React from 'react';
import { useParams } from 'react-router-dom';

import './App.css';

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Product'
import AllProducts from './components/AllProducts'
import AllOrders from './components/AllOrders'
import UpdateProduct from './components/UpdateProduct'

import BuyProduct from './components/BuyProduct'
import Cart from './components/Cart'
import CreateProduct from './components/CreateProduct'
import Admin from './pages/Admin'
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import { UserProvider } from './UserContext'




function App() {

  const [cartItems, setCartItems] = useState([]);
  const [productId, setProductId] = useState(null);
  const [ userId, setUserId] = useState(null);
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      fetch(`https://e-commerce-backendapi.onrender.com/users/profile`,{
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(data => {
        console.log(data)
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      });
    } else {
      setUser({
        id: null,
        token: null,
        isAdmin: null
      });
    }
  }, []);
  

  useEffect(() => {
    fetch(`https://e-commerce-backendapi.onrender.com/users/${userId}/cart`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      setCartItems(data);
    });
  }, [userId]);


  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavbar />
        <Container fluid>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/products" element={<Products />} />
            <Route path="/allproducts" element={<AllProducts />} />
            <Route path="/allorders" element={<AllOrders />} />
            <Route path={`/updateproduct/:productId`} element={<UpdateProduct productId={productId} />} />
            <Route path={`/products/:productId`} element={<BuyProduct productId={productId} />} />
            <Route path="/create" element={<CreateProduct />} />
            <Route path={`/:userId/cart`} element={<Cart userId={userId} cartItems={cartItems} />} />
            <Route path="/admin" element={<Admin />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="*" element={ <Error /> } />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}


export default App;
