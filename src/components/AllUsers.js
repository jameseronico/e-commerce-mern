import React, { useEffect, useState } from 'react';
import { Button, Row, Table } from 'react-bootstrap';

export default function AllUsers() {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    fetch(`https://e-commerce-backendapi.onrender.com/users/allusers`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setUsers(data);
      });
  }, []);

  const setAsAdmin = (userId) => {
    fetch(`https://e-commerce-backendapi.onrender.com/users/setadmin/${userId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // update the user's isAdmin property in the state
        const updatedUsers = users.map((user) =>
          user._id === userId ? { ...user, isAdmin: true } : user
        );
        setUsers(updatedUsers);
      })
      .catch((error) => {
        console.error('Error setting user as admin:', error);
      });
  };

  return (
    <Row className="mx-5 px-5 alluser-page">
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>Email</th>
            <th>isAdmin</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user) => (
            <tr key={user._id}>
              <td>{user._id}</td>
              <td>{user.email}</td>
              <td>{user.isAdmin ? 'Yes' : 'No'}</td>
              <td>
                {!user.isAdmin && (
                  <Button variant="primary" onClick={() => setAsAdmin(user._id)}>
                    Set as Admin
                  </Button>
                )}
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Row>
  );
}
