import React, { useEffect, useState } from 'react';
import { Button, Row, Table } from 'react-bootstrap';
import { useParams } from 'react-router-dom';

export default function AllUsers() {
  const [users, setUsers] = useState([]);

  const [orders, setOrders] = useState([]);

  useEffect(() => {

        fetch(`https://e-commerce-backendapi.onrender.com/users/orders`, {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
        })
        .then((res) => res.json())
        .then((data) => {
          console.log(data)
          setOrders(data);
        });

  }, []);


 

  return (
    <Row className="mx-5 px-5 allorder-page">
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>Products</th>
            <th>Total Amount</th>
          </tr>
        </thead>
        <tbody>
          {orders && orders.map((order) => (
            <tr key={order._id}>
              <td>{order.userId}</td>
              <td>
                <ul>
                  {order.products.map((product) => (
                    <li key={product._id}>
                      {product.productName} x {product.quantity} = {product.subtotal}
                    </li>
                  ))}
                </ul>
              </td>
              <td>{order.totalAmount}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Row>

  );
}
